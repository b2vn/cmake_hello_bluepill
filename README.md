# STM32 Docker

A docker container that contains the requirements to build an application for
a BluePill (STM32F103C8).

## Usage

Run the container

```console
docker run -it --rm -u`id -u`:`id -g` -w`pwd`:/code -w/code registry.gitlab.com/b2vn/cmake_hello_bluepill:latest
```

Inside the container, the sample code builds like any other cmake project using
ninja

```console
mkdir build
cd build
cmake ..
ninja
```

This will produce the target file in `src/hello_bluepill.elf`. This project
does not contain a programmer for the target, e.g. use STMCubeIde to flash you
target.

## Flashing the target

This project does not come with any means to flash the target (the BluePill).
The tool you should use will depend on the programmer you have, but you can
e.g. use [ST-Link Utility](https://www.st.com/en/development-tools/stsw-link004.html).

## Build the container

Thought this is not the intended use for the project and tool chain, if you
wish to build the container locally, the dockerfile and docker-compose files
are supplied.

Build the container

```console
docker-compose build
```

Run the container

```console
docker-compose run BluePill
```

## Inside the container

This section is a short description of what to find in this project.

### Toolchain

The toolchain is provided by

* The official ARM gcc release
* The official STM abstraction layer for Cube
* ObKos stm32 cmake

For links and specific version, please see the args in the top of the
`dockerfiles/Dockerfile`.

Furthermore, the following packages are provided by the distro ppa

* cmake v3.16.3
* ninja v1.10.0

### User privileges

The `uid` and `gid` used inside the container are specified `.env`. Default is
1000.

### Aliases

The container comes with a few aliases for ease of you. You might need to
modify them to fit you exact need. See `dockerfiles/bashrc` for details.

## Demo code

The application being built in the example is designed with STM32CubeIde and
for a BluePill (a tiny demo board with an STM32F103C8).

It simply just blink the on-board LED, connected to GPIO_C13.

There is not a lot to say about the code it self - it was generated with
CubeIde, and the auto generated source files copied into this project. As
default, CubeIde will generate these files into a folder called `Core`, but
note that not all files can be used with this tool chain, or just leave those
specific files out of the `CMakeLists.txt`.
